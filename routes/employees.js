var jsonfile = require('jsonfile');
var _ = require('underscore');
var file = './files/employees.json';

module.exports = function(app) {
  //get employees from json file
  app.get('/employees',function(req,res){
    jsonfile.readFile(file, function(err, obj) {
        res.send(obj);
    })
  });

  //insert employee in json file if another employee with
  //this firstname and surname doesn't exist
  app.post('/employee',function(req,res, next){
    var employee = req.body;
    jsonfile.readFile(file, function (err, employees) {
      if (err) return next(err);
      if(checkAndAdd(employee, employees)) {
        jsonfile.writeFile(file, employees, function (err) {
            if (err) return next(err);
            console.error(err);
            res.send('The employee has been successfully added.');
        })
      } else {
         res.status(400);
         res.send('Employee with this firstname and surname already exists.');
      }
    })
  });

  //upodate an employee in json file
  app.put('/employee',function(req,res){
    var putObj = req.body;
    jsonfile.readFile(file, function(err, employees) {
      updatePropertyInObj(putObj.property,putObj.oldValue,putObj.newValue,employees);
      jsonfile.writeFile(file, employees, function (err) {
          if (err)
            console.error(err);
          else {
            res.send('Employee has been successfully updated');
          }
      })

    });
  })

   //delete an employee  from json file for provided firstname and surname
  app.delete('/employee/:firstname/:surname',function(req,res){
    var employee = req.params;
    jsonfile.readFile(file, function(err, employees) {
      var newArrayWithEmployees = employees.filter(function(obj) {
               return obj.firstname !== req.params.firstname && obj.surname !== req.params.surname;
       });
        jsonfile.writeFile(file, newArrayWithEmployees, function (err) {
          res.send('Emmployee has been successfully deleted');
      })
    })
  });

  function updatePropertyInObj(property,oldValue,newValue,jsonObjArr){
      for (var i=0; i<jsonObjArr.length; i++) {
          if (jsonObjArr[i][property] === oldValue) {
              jsonObjArr[i][property] = newValue;
              return;
          }
        }
  }

function checkAndAdd(newEmployee, employeesArr) {
    var successfullyAdded = true;
    if (!checkIfExists(newEmployee, employeesArr)) {
      employeesArr.push(newEmployee);
    } else {
      successfullyAdded = false;
    }
    return successfullyAdded;
}

function checkIfExists(newEmployee, employeesArr){
  return  employeesArr.some(function (employee) {
      return employee.firstname === newEmployee.firstname && employee.surname === newEmployee.surname;
  });
}

}
