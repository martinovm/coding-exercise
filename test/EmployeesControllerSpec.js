describe('EmployeesController', function() {
      var $controller,
        $scope,
        employeesService,
        validationService,
        uiGridValidateService,
        employeesServiceGetEmployeesPromise;


    beforeEach(module('gamesysApp.controllers'));

    beforeEach(function () {
      var mockEmployeesService = {};

      validationService = jasmine.createSpyObj('validationService', [
        'validateAlphanumeric'
      ]);

      uiGridValidateService = jasmine.createSpyObj('uiGridValidateService', [
        'setValidator'
      ]);



      module(function ($provide) {
        $provide.value('validationService', validationService);
        $provide.value('employeesService', mockEmployeesService);
        $provide.value('uiGridValidateService', uiGridValidateService);
      });
      inject(function($q) {
       mockEmployeesService.data =  [
          { firstname: "Bob",
            surname:"Marley",
            contact_number:"09872 089 456",
            email:"BobMarley@jamaica.com" },
         {  firstname:"Jim",
            surname:"Carey",
            contact_number:"+44 788 877 899",
                 email:"Jim.Carey@themask.com"
         }];

     mockEmployeesService.getEmployees = function() {
       var defer = $q.defer();

       defer.resolve(this.data);

       return defer.promise;
     };


   });
});

    beforeEach(inject(function(_$controller_, $rootScope, $q, _validationService_) {
      validationService = _validationService_;
      $scope = $rootScope.$new();

      $controller = _$controller_('employeesCtrl', {
        $scope: $scope,
        validationService: validationService
      });
      $scope.$digest();
    }));


  it('should be defined', function () {
    expect($controller).toBeDefined();
  });

  it('$scope.gridOptions.headerTemplate should be defined', function () {
     var expected  = './views/header-template.html';
     expect($scope.gridOptions.headerTemplate).toEqual(expected);
  });

  it('getEmployees()  to have been called and $scope.gridOptions.data populated', function () {
    $scope.loadEmployees();
    $scope.$digest();
    var expected = [
      { firstname: "Bob",
        surname:"Marley",
        contact_number:"09872 089 456",
        email:"BobMarley@jamaica.com" },
     {  firstname:"Jim",
        surname:"Carey",
        contact_number:"+44 788 877 899",
             email:"Jim.Carey@themask.com"
     }];
    expect($scope.gridOptions.data).toEqual(expected);
  });

});
