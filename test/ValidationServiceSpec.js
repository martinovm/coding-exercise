describe('ValidationService', function() {
  var validationService;
  beforeEach(function() {
    var $injector = angular.injector(['gamesysApp.services']);
    validationService = $injector.get('validationService');
  });
  it('should return false for invalid email and true for valid', function() {
    var invalidEmail = 'email@domain';
    var validEmail = 'email@domain.com';
    var result = validationService.validateEmail(invalidEmail);
    expect(result).toBeFalsy();
    result = validationService.validateEmail(validEmail);
    expect(result).toBeTruthy();
  });

  it('should return false for invalid firstname and surname and true for valid', function() {
    var invalidSurname = 'Peter*';
    var validSurname = 'Peter';
    var result = validationService.validateAlphanumeric(invalidSurname);
    expect(result).toBeFalsy();
    invalidSurname = 'Peter ';
    result = validationService.validateAlphanumeric(invalidSurname);
    expect(result).toBeFalsy();
    result = validationService.validateAlphanumeric(validSurname);
    expect(result).toBeTruthy();
  });

  it('should return false for invalid contact number and true for valid', function(){
    var invalidContactNumber = '07890';
    var validContactNumber = '0789 0020003035';
    var result = validationService.validateContactNumber(invalidContactNumber);
    expect(result).toBeFalsy();
    result = validationService.validateContactNumber(validContactNumber);
    expect(result).toBeTruthy();

  });



});
