describe('EmployeesService', function() {
  var employeesService, httpBackend;

  beforeEach(function() {
     module('gamesysApp.services');
     inject(function (_$httpBackend_,_$http_,_employeesService_) {
        httpBackend = _$httpBackend_;
        employeesService = _employeesService_;
      });
    });

  it('should be called', function () {
            var employeesData;
            var expectedEmployeesData = [
               { firstname: "Bob",
                 surname:"Marley",
                 contact_number:"09872 089 456",
                 email:"BobMarley@jamaica.com" },
              {  firstname:"Jim",
                 surname:"Carey",
                 contact_number:"+44 788 877 899",
                      email:"Jim.Carey@themask.com"
              }];


            httpBackend.when('GET', '/employees').respond(expectedEmployeesData
            );
            employeesService.getEmployees().then(function (data) {
                employeesData = data;
            });
            httpBackend.flush();
            expect(employeesData.length).toEqual(2);
        });


});
