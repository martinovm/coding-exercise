
var app = angular.module("gamesysApp", [
  'gamesysApp.controllers', 'gamesysApp.services','ngRoute','ui.grid', 'ui.grid.edit', 'ui.grid.cellNav', 'ui.grid.validate','ngMessages'])
  .config(['$routeProvider',
    function($routeProvider) {
      $routeProvider.
      when('/employees', {
        templateUrl: '/views/employees.html',
        controller: 'employeesCtrl'
      }).
      otherwise({
        redirectTo: '/employees'
      })
    }
  ])
