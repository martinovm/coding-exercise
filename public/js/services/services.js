angular.module('gamesysApp.services', [])
.factory('employeesService', function($http) {
    return {
      getEmployees: function() {
        return $http.get('/employees').then(function(result) {
            return result.data;
        });
      },
      createEmployee: function(employee) {
        return $http.post('/employee',employee).then(function(result) {
            return result.data;
        });
      },
      updateEmployee: function(employee) {
        return $http.put('/employee',employee).then(function(result) {
            return result.data;
        });
      },
      deleteEmployee: function(employee) {
        return $http.delete('/employee'+'/'+employee.firstname+'/'+employee.surname,employee).then(function(result) {
             return result.data;
        });
      }


    }
 })
 .factory('validationService', function() {
     return {
       validateAlphanumeric: function(value) {
                 var regexp = /^[a-zA-Z0-9]+$/;
                 return regexp.test(value);
       },
       validateEmail: function(value) {
                 var regexp = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
                 return regexp.test(value);
       },
       validateContactNumber: function(value) {
                 var regexp = /^[0-9\+\s]{7,15}$/;
                 return regexp.test(value);
       }
   }


  })
