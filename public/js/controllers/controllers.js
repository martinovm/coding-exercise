angular.module('gamesysApp.controllers', [])
.controller('employeesCtrl',
  function($scope, $http, employeesService, validationService, uiGridValidateService, $window, $q, $interval, $timeout) {
      //add validation patterns for the new employee form
      $scope.alphanumericPattern = (function() {
            return {
              test: function(value) {
                return validationService.validateAlphanumeric(value);
              }
            };
          })();
      $scope.emailPattern = (function() {
            return {
              test: function(value) {
                return validationService.validateEmail(value);
              }
            };
          })();
      $scope.contactNumberPattern = (function() {
            return {
              test: function(value) {
                return validationService.validateContactNumber(value);
              }
            };
          })();

      //add validation patterns to the grid for cells validation
      uiGridValidateService.setValidator('alphanumeric',
      function() {
        return function(oldValue, newValue, rowEntity, colDef) {
          if (!newValue) {
            return true;
          } else {
            return validationService.validateAlphanumeric(newValue);
          }
        };
      },
      function(argument) {
        return 'You can only insert alphanumeric value';
      }
    );

      uiGridValidateService.setValidator('contactNumber',
      function() {
        return function(oldValue, newValue, rowEntity, colDef) {
          if (!newValue) {
            return true;
          } else {
            return validationService.validateContactNumber(newValue);
          }
        };
      },
      function(argument) {
        return 'You can only insert valid Contact Number';
      }
    );

      uiGridValidateService.setValidator('email',
      function() {
        return function(oldValue, newValue, rowEntity, colDef) {
          if (!newValue) {
            return true;
          } else {
            return validationService.validateEmail(newValue);
          }
        };
      },
      function(argument) {
        return 'You can only insert valid email';
      }
      );

      //add customized header template for the load button and the table title
      $scope.gridOptions = {enableCellEditOnFocus: true,
        headerTemplate: './views/header-template.html'};

      //add employee
      $scope.addEmployee = function() {
        var employee = {
          firstname: $scope.firstName,
          surname: $scope.surName,
          contact_number: $scope.contactNumber,
          email: $scope.email
        };
        employeesService.createEmployee(employee).then(function(data) {
              //if successfully created it will be added to the top of the grid
              $scope.gridOptions.data.splice(0, 0,
                employee
              );
              $scope.message = data;
            },

            //if employee exists with this firstname and surname it will show an
            //error message
            function(errResponse) {
              $scope.message = errResponse.data;
            }
        );
      };

      //delete employee
      $scope.deleteEmployee = function(row) {
              var index = $scope.gridOptions.data.indexOf(row.entity);

              //delete employee from the grid
              $scope.gridOptions.data.splice(index, 1);
              employeesService.deleteEmployee(row.entity).then(function(data) {
                    $scope.message = data;
                  },
                  function(errResponse) {
                    $scope.message = errResponse.data;
                  }
              );
            };

      //deffine grid columns
      $scope.gridOptions.columnDefs = [
        {name: 'firstname', displayName: 'First Name', width: '20%',validators: {required: true, alphanumeric: true, maxLength: 50}, cellTemplate: 'ui-grid/cellTitleValidator'},
        {name: 'surname', displayName: 'Surname', width: '20%',validators: {required: true, alphanumeric: true, maxLength:  50}, cellTemplate: 'ui-grid/cellTitleValidator'},
        {name: 'contact_number', displayName: 'Contact Number', width: '20%',validators: {required: true, contactNumber: true, minLength: 7, maxLength:  15}, cellTemplate: 'ui-grid/cellTitleValidator'},
        {name: 'email', displayName: 'Email', width: '20%',validators: {required: true, email: true}, cellTemplate: 'ui-grid/cellTitleValidator'},
        {name: 'Delete',
            cellTemplate: '<button class="btn primary" ng-click="grid.appScope.deleteEmployee(row)">X</button>'}
      ];

      //load employees into the grid on click of the load button
      $scope.loadEmployees = function() {
          employeesService.getEmployees().then(function(data) {
                $scope.gridOptions.data = data;
              },
              function(errResponse) {
                console.error('Error while fetching employees');
              }
          );
        };

      $scope.gridOptions.onRegisterApi = function(gridApi) {
        $scope.gridApi = gridApi;
        gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
          if (oldValue  == newValue) {
            return false;
          } else {
            //check if the employee with this firstname and surname exists
            if((colDef.name == 'firstname' || colDef.name =='surname') && checkIfExistsTwiceInGrid(rowEntity, $scope.gridOptions.data) == 2){
              $scope.message = 'Employee with this firstname and surname already exists.';
              return false;
            }
            $timeout(function() {
              //call the update service only if no validation errors
              if (!$scope.gridApi.validate.isInvalid(rowEntity, colDef)) {
                employeesService.updateEmployee({property: colDef.name,newValue: newValue,oldValue: oldValue}).then(function(data) {
                  $scope.message = data;
                });
              }
            }, 0);
          }

        });
      };

      function checkIfExistsTwiceInGrid(updatedEmployee, employeesArr){
        var tempEmployeesArr = employeesArr.slice();
        var count =0;
        tempEmployeesArr.forEach(function (employee) {
            if(employee.firstname === updatedEmployee.firstname && employee.surname === updatedEmployee.surname){
                count++;
            }
        });
        return count;
      }
    }
);
