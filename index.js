// VARIBALES
var express = require('express');
var bodyParser = require('body-parser');
var fs = require("fs");


var app = express();

app.use( bodyParser.json() );
app.use(bodyParser.urlencoded({   
  extended: true
}));


app.use('/', express.static(__dirname + '/'));
app.use('/assets', express.static(__dirname + '/public'));
app.use(express.static('public'));



var routePath="./routes/";
fs.readdirSync(routePath).forEach(function(file) {
    var route=routePath+file;
    require(route)(app);
});

var port = process.env.PORT || 3000;
var server = app.listen(port, function () {
    console.log('Server running on port ' + port);
});
